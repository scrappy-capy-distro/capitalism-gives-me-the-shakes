\null
\makesection{A Note On the Text}

I don't usually edit the zines I publish. I put out a certain number of copies and then take them out of circulation, preferring to make new ideas. This zine will be different and will be edited as a reflection of how anxiety is always changing and adapting.

I'm envious (and suspicious) of those people who've been ‘cured.' Part of my apprehension about writing this zine was thinking that I was the last person to write this because my mental health seems to go through stages of okay to truly fucked.

Without trying to be a total bummer upfront, I think it's pretty natural to struggle right now or to have struggled for a while. We live in such a beautiful world and the people, animals and resources are being actively murdered, manipulated, exploited and destroyed for greed. It's kind of fucking hard not to take that on board sometimes.

The following zine is not about getting cured or becoming normal (whatever the fuck that is). Both because this isn't really a helpful way to think and because I think, for a lot of people, that ``normal'' might mean complacent or docile. Instead, this is about tips for learning to live with anxiety, to try to cope with it so that we have the ability to resist, fight; to try to make the world a little better in whatever ways we can.

\clearpage\null

\makesection{So Qualified}

I'm not usually a huge bragger but man, I am so qualified to write about experiences of anxiety.
I've had mild stuff (even now writing this: tight chest, twitching leg, weird vision, thoughts going round and round and round) and I've had heavy stuff (racing heart, nausea, the shits, twitching legs, full-on doom and gloom, my own mortality clicking down in front of me like the flashing red timer on a bomb).

As a qualification for the advice I'm giving --- well, that I don't have, but since anxiety and panic attacks kicked in ten years ago I've tried: reading a lot of self-help books; taking antidepressants; stopping antidepressants; getting high; getting drunk; getting sober.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth,height=12cm,keepaspectratio]{textured-leaves}
\end{figure}

The pandemic, shockingly, didn't help. As someone who already leans (runs with outstretched arms) towards anxiety, it's pretty hard to just compartmentalise it into the correct areas that it should be without having it pop up everywhere else. It's making simple things seem impossible, it's making me feel that I need to hold tightly onto the idea of control while feeling totally helpless.

Anxiety has caused me to leave restaurants, leave countries, leave relationships, stare at my phone, hide my phone, and flip out over minor decisions. I've had panic attacks on planes, in restaurants, in bars, and in every bedroom I've been in for the last ten years.

I've avoided a lot of political actions, fearing that I'd be a liability wherever I went and would need to be helped by people who were there to help others. A zine about mutual aid%
\footnote{``Insurrectionary Mutual Aid.'' Curious George Brigade.}
talks about preparedness when showing up to crises, and the line ``Any insurrectionist needs to be self-sufficient in the basics'' jumped out at me. For all my fellow anxious comrades and friends, this is a zine about trying to be self-sufficient, ie, not freaking the fuck out whenever we leave the apartment, let alone helping out/getting into mischief.

Following are some of the things that have helped me, especially in the last scary and sick months. Laura and I have talked about a lot of these tactics together (she is also so qualified), and she's one of the first people I call when having a panic attack because she knows what to say. There's no one else I would make this zine with.

\clearpage
\pagestyle{empty}

\begin{tikzpicture}[remember picture,overlay]
    % Place the image first
    \node[anchor=center] at (current page.center) {\includegraphics[width=\paperwidth,height=\paperheight,keepaspectratio]{sun-rays}};
\end{tikzpicture}

\clearpage

\begin{tikzpicture}[remember picture,overlay]
    % Place the image first
    \node[anchor=center] at (current page.center) {\includegraphics[width=\paperwidth,height=\paperheight,keepaspectratio]{panic-attacks}};

    % Place the text slightly lower
    \node[anchor=north, text width=\textwidth, align=center, yshift=-2cm, font=\headingfont\fontsize{30}{0}\selectfont] at (current page.north) {PANIC ATTACKS};
\end{tikzpicture}

\clearpage
\pagestyle{main}

These are some tips for when you're in the midst of a panic attack, or when you feel one coming on.

\textbf{First --- you're going to be okay.}
You're not going to die. You're not going to go insane. Thinking that you're going to die or go insane are symptoms of a panic attack. A panic attack can't kill you, and it can't make you crazy.
You're going to be uncomfortable for the next little while --- that's all.
The worst thing that can happen is that you'll be very uncomfortable. That's okay! Being uncomfortable is not dangerous. Instead of saying dramatic things to yourself, try replacing it with “I'm very uncomfortable.”

\textbf{Try to stay where you are.}
Unless you're in actual danger, try to stay where you are, just for five minutes more, and five minutes after that. The place isn't dangerous, it's just your body saying it is. There are two reasons to stay:

\begin{figure}[p]
    \thisfloatpagestyle{empty}
    \vspace{-12mm} % undo the top spacing from regular geometry
    \centering
    \includegraphics[width=\paperwidth,height=\paperheight,keepaspectratio]{tree-canopy}
\end{figure}

\begin{enumerate}
\item Because you're proving your panic attack wrong.  The supermarket (or wherever you are) isn't dangerous. You can stay, but you will be uncomfortable.

\item The next time your panic attack tells you something is dangerous; you can tell it: “But you said that last time and everything was fine.” You take power away from these thoughts.
This will be uncomfortable, but it will not be dangerous.
It will end. Whatever you do, it will end. You don't need to chase calm. Calm will come to you, it's just a matter of time.
\end{enumerate}

\textbf{Accept what's happening.}
Don't be mad at yourself. Don't yell at yourself to stop. Be kind to yourself. Tell yourself you will be okay --- because you will. The panic attack will end and calm will come.

\textbf{Stop trying to control it. Let yourself feel it.}
Trying to control the panic isn't helping. Let yourself feel the unpleasantness. Nothing bad will happen to you if you accept that you're panicking. You might feel more emotion, but you might also feel better more quickly, or lessen the intensity of it. Again --- you will fill moderately to extremely uncomfortable, but you will be fine.

\textbf{Notice the waves.}
Is the panic one big burst, or are there moments of calm in there? Notice the lower points. These lower points will become longer and longer until the panic subsides. Take those moments to breathe.

\textbf{Drink/eat.}
Take some little sips of water, and if you haven't eaten in a while, then get something a bit bland and take small bites. Sometimes this can reassure your body that you're not in danger, because if you were really in danger you wouldn't be pausing to drink or eat.

\textbf{Rest.}
Let yourself rest after you've experienced this because it is very tiring. Be kind to yourself, and maybe reach out to a friend about it.

{\fontsize{12}{12}\selectfont\textbf{\MakeUppercase{For Next Time}}}

The next time you feel the panic attack building, try to change your attitude towards it. By the end of this zine, I hope you'll have some different strategies. Instead of reacting to the panic attack with fear, try to tell yourself that you want it to come and that you want to try out these strategies.

Of course, it's all lies. Nobody wants a panic attack. But it helps to tell yourself this. Who knows, maybe one day you'll believe it.

\clearpage\null

\makesection{Tips for Bystanders}

\begin{enumerate}
\item \textbf{Be calm} (even if you don't feel calm). Tell the person panicking that you understand that they're feeling really uncomfortable but tell them you're not worried they're going to die or go crazy. Reassure them that it will pass soon and that you're there.

\item \textbf{Talk.} It depends on the person, but if they haven't told you to shut up, try talking to them in a calm way. Stay away from difficult long conversations or stories, because when the person is panicking it's hard to concentrate. Maybe tell them about a small, funny thing, or say “Isn't it bright today/look at the moon/isn't it cold? Hot?” This can help them focus away from their body, and might help if they feel ‘unreal,' or not a part of reality.

\item \textbf{Stay.} If you're somewhere busy (and if you're in lockdown you absolutely shouldn't be somewhere busy), you could lead them somewhere a little quieter, tell them that you can leave but encourage them to stay.

\item \textbf{Reassure.} Tell them that you're there for them, that they're safe, and that all that will happen is they will feel intensely uncomfortable for the next 5--20 minutes. Tell them that they're doing well.

\item \textbf{Ask.} Do they have any coping methods? Do they have something that would make them feel comfortable?

\item \textbf{Understand.} If they snap at you or seem annoyed, don't take offence --- they're going through a really tough time.

\item \textbf{Appreciate.} This person is being really vulnerable with you. Take it as a compliment --- if they're able to show you this side of you they probably trust you.

\item \textbf{Afterwards.} Offer to walk them home, or stay with them for the next while, because they'll probably be very tired and fragile. If they're up for it, ask for feedback as to what would be helpful next time (or tell them to tell you another time if they're too tired).
\end{enumerate}

\clearpage

{\fontsize{12}{12}\selectfont\textbf{\MakeUppercase{Don't}:}}

\begin{enumerate}
\item Tell them to get over it, or tell them that they're being stupid, or that there's nothing to be scared of. By the time it's a panic attack the body is flooding them with stress, their heart is pounding, they might feel nauseous, feel dizzy, feel faint. Even if you don't understand it, and if you're lucky enough to have never experienced it, it definitely is something.

\item Expect them to be full of energy, ready to jump right back into whatever it was you were doing before the panic attack. They'll probably want to sleep, or rest, because panicking is incredibly exhausting! Take them home, make them a cup of tea and, most important of all, don't judge them.
\end{enumerate}

\clearpage\null

\thispagestyle{empty}
% Title and Image on the Same Page (Text Lower, No Page Number)
\begin{tikzpicture}[remember picture,overlay]
    % Place the image first
    \node[anchor=center] at (current page.center) {\includegraphics[width=\paperwidth,height=\paperheight,keepaspectratio]{panic-attacks}};

    % Place the text slightly lower
    \node[anchor=north, text width=\textwidth, align=center, yshift=-2cm, font=\headingfont\fontsize{30}{0}\selectfont] at (current page.north) {ANXIETY};
\end{tikzpicture}

\clearpage\null

\makesection{The Critic and the Doctor, Or: \\Challenging the Voices}

I call the shitty and judgemental voice in my head The Critic. Probably if you're reading this you have it too.

The Critic says that there's something wrong with being anxious and having panic attacks, that it's embarrassing, and that I should keep it to myself. It wants me silent and uncommunicative in case I say the wrong thing.

The Critic doesn't want me to write these things, partly because it wants to convince me that it's an integral part of me that can't be challenged, and partly because it knows that vulnerability and openness go a long way towards helping anxiety and panic attacks.

I had another voice too. This was different, or so I thought. This one seemed to have a medical background. This one warned me that my anxiety was getting too bad, and there wasn't going to be much joy in life, and that each panic attack was making my brain so fragile that it would soon shatter.

I had terminal anxiety, it said, and I believed it. I would die from it or go insane from it. I had to be more careful, I had to try to control more. If I could just control everything, I'd be safe.

I listened to this voice for a long time. The things it said hadn't come true, but I believed they would. I didn't associate it with The Critic, because it seemed so different.

When I was talking about it with my therapist, he said that they sounded like the same voice. I told him I didn't think so, that this other medical voice seemed like it knew what it was talking about. But I started to observe it in the same way that I was observing The Critic.

I started to separate this voice, and I started to call it The Doctor. Not a good kind of doctor, the kind who seems to really care, but something else, something nervous, something that was desperate to hang onto its power. Someone who, it turns out, was full of shit.

A few days after a therapy session I was getting ready for work and I noticed the same old tightness in the chest, the shaking hands.

``Oh, you're anxious,'' said The Doctor. ``That's no good. Probably a sign that you're going to be anxious forever\ldots{} I think it'll only get worse from here on out.''

I paused. ``Weird. That sounded a lot like something The Critic would say.''
The Doctor cleared his throat and spoke in a deeper tone. ``No, no, you're mistaken. I know what I'm talking about. What would you know about it, dummy?''
I looked closer. ``Hang on --- that stethoscope is a kid's toy! And show me your medical identification card. Give it! I'm sorry --- it says here you went to `Herverd Medical School'?''

The Critic and The Doctor were the same fucking voice! I realised then that this voice didn't want me safe, and it didn't want to protect me. It wanted me hidden.

Now I challenge The Doctor the same way that I'd been challenging The Critic. I'm not scared of him anymore, and I don't believe what he says. I'm not going to die from a panic attack, and anxiety won't slowly send me crazy.

When The Doctor whispers in my ear that I'm anxious, I go through the list. Am I hungry, tired or in pain? No. Okay. Have I reached out to friends today? Yes. Then maybe I'm anxious, but instead of saying that to myself over and over again, I've replaced it with: I'm uncomfortable. That's fine. I can be uncomfortable.

Since COVID, I've had some health issues. It's so hard to have anxiety and health issues at the same time --- a lot of doctors dismiss you, and also, there's the cycle of getting anxious about the issues, which can make the issues worse. One thing I tell myself when my chest feels tight, is when I wonder if the illnesses are getting worse --- anxiety isn't a symptom of the issue. It helps me to separate the two things.

I don't put faith in The Doctor anymore, I don't believe that The Doctor has my best interests at heart. If he tells me that I'm always going to be anxious, I tell him that he's told me this before, that he's boring, and that I can look after myself.

\begin{figure}[p]
    \thisfloatpagestyle{empty}
    \vspace{-12mm} % undo the top spacing from regular geometry
    \centering
    \includegraphics[width=\paperwidth,height=\paperheight,keepaspectratio]{leaf-on-twig}
\end{figure}

\clearpage\null

\makesection{The Losers' Club, Or: \\We Need to Talk}

When I'm trapped in the cycle of anxious thoughts, I write them down. The worst-case scenarios are there, and I don't have to be stuck anymore thinking about what could happen. A lot of times, the stuff seems absurd once I've written it down, or I realise that it's not even in alignment with the views I have.

It also helps to identify what I'm feeling. The Doctor spent too many years telling me that I was anxious because it didn't want me to feel anything else --- pain, grief, jealousy. Labelling it as anxiety meant that I didn't have to acknowledge or deal with these emotions. It meant that I could push them down and not open up to people or talk to anyone. It also ensured a never-ending cycle of suppressing feelings and experiencing anxiety.

I do this with talking as well. It's easier for me to say I'm anxious than \textit{I'm lonely, I'm intimidated, I'm embarrassed and stressed about money (and annoyed that I'm letting it get to me)}. It's easier than saying \textit{I miss going out but I'm scared of getting or giving COVID}. Easier than saying \textit{I want to date more but I feel like a fucken bummer because of (see above)}. Easier than saying \textit{I feel disposable}.

And fuck --- I was brought up as the gender that made this stuff, or at least emotions, okay or encouraged to talk about. I wanna know how people raised as men are able to vent about this shit. I've heard stories about anger being the only available option, sadness being viewed as a bummer or (even in radical circles).

Which, who knows, probably also gets shit done, except that they're missing out on how fucking cathartic crying feels, and given so many men* are vulnerable (or try to be) with partners, it'd be nice if they could be with their male friends.

I guess what I'm trying to say is that I want us (i.e. me) to move beyond I'm sad, I'm angry. To identify the exact reasons, to reach out to other people to see if they're feeling the same. I want honesty with each other, times spent talking in detail about how shit all of this is. It might take away a lot of our mystique but I think it's time we stopped caring so much about being perceived as cool.

\clearpage\null

\makesection{You Don'T Need Curing, Or: \\Adjust Your Aims}

I went into therapy with the expectation that I would be able to get rid of anxiety and panic attacks completely. This meant that whenever I felt anxiety or had a panic attack, it meant that the therapy wasn't working, that nothing would work, and that the anxiety would just keep spiralling down forever until either my heart or my brain exploded.

I know now that this attitude, this idea that anxiety automatically means a panic attack, or that anxiety after a long stretch of calm should be worried over isn't helpful. Besides, what is the real opposite of anxiety? I used to think that it was numbness. It's why I drank and used drugs. I was desperately trying to find something that would shut the voice up, would let me have a moment's rest.

I don't think a good treatment is numbness. I think it's acceptance and vulnerability. Accepting that I feel anxiety, accepting that when things get rough, anxiety and panic attacks are something that will come up. The Critic and The Doctor both expect perfection, and I'm a fallible person and perfection is boring. Understanding and embracing this idea helps a lot.

\clearpage\null

\makesection{Fuck Happiness, Or: \\Reject Perfect Expectations}

I have no time for happiness. What does it even mean? What does happiness have to do with learning? Happy is something you buy, it's not something you earn or discover. Happy is a bullshit capitalist concept. We don't learn from happiness. We don't progress with happiness.

Happiness doesn't help anxiety, either. This idea of happiness leads to endlessly comparing, endlessly thinking --- if I made a different choice, I'd be here in life, and I'd be happy. If I wasn't such a dickhead, I'd be happy. If I hadn't sent that dumb text, I'd be happy. If I could only stop having anxiety and panic attacks, I'd be happy.

Also, how entitled is it, to expect to be happy all the time, when there's so much injustice everywhere? For some people, everything is not going to be okay, and when this pandemic is over, people's lives will also be full of prejudice, pain, and isolation, even if they ‘put on a happy face.'

Fuck happy!

I want to be sated, excited, fulfilled, curious, compelled, grateful, challenged, entertained. I even want to be sad sometimes, I want to be angry, I want to feel pain. I want to be fucking furious about injustice and apathy, about people wasting their whole glorious lives working for shitty companies that exploit them. I want to grow, and I want to learn. This means I won't be happy, because happy is static.

Also, seriously --- you're not happy? No shit! Horrible things are happening! That means you're empathetic, it means you're paying attention, because a lot of people are suffering, and a lot of worldwide shitty practices are even more obvious and people are showing their indifference, spending all their energy on these fucking conspiracy theory marches instead, or talking about how they're grateful they have this time at home.

If you still want to frame your goals with happiness, then go ahead, but maybe don't look for happiness in a pandemic. It's really okay to not be okay right now. If you are okay, or doing well, then put on a mask and go see how your neighbour is doing.

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{happiness}
\end{figure}

And now, in 2023, think of the people who are still stuck at home with compromised immune systems, who have been totally left behind now that COVID is ‘over.' How can we help them and include them? Are we really throwing people away so that we can go clubbing, eat in restaurants all the time, and not have the courtesy of taking some time away to spend time with them? This could have been an opportunity to think of others; it was right in front of us.%
\footnote{For more of a critique on anarchists' reactions to the pandemic, read the zine ``Pandemic Praxis'' from Scrappy Capy distro.}

\clearpage\null

\makesection{Be Prepared But Also \\Not Too Prepared, Or: \\Build Up a Practice}

I always carry around a backpack. It has stuff in it that I like: my journal, my notebook, pens, meds, a little bag with period supplies (and now, disinfectant and a spare mask). I also have a couple of things that comfort me if I'm feeling anxious. I have headphones in case I'm overwhelmed and want to listen to music or podcasts, and I have Valerian tablets with me because they help me.

Basically, I like to maintain the idea that I'm spontaneous by being really neurotically prepared.

Outside of this, I'm trying to prepare in other ways. I write down three things I'm grateful for each night and I try to look out for them during the day. When I'm in moments of calm, I breathe in for four, hold for four, and breathe out for five. I meditate. These are tools that I can use when I feel anxiety coming in.

I try not to be too reliant on these things. I know that a panic attack won't kill me because I forgot my headphones that one time. I try instead to build up the other practices so that I can breathe, and I can look for things to be grateful for, even in the middle of panicking.


\begin{figure}[hb]
    \centering
    \includegraphics[width=\textwidth]{saplings}
\end{figure}

\clearpage\null

\makesection{Self-Care Without the Bath Bombs, Or: \\Doing the Hard Work}

I think we all know by now that self-care isn't about buying a bunch of useless stuff. I think self-care is about looking after myself and being kind, but it's also about doing hard work. It's trying to maintain and nurture honesty and boundaries.

It's acknowledging when I'm acting out of fear, or jealousy. It's about knowing when I'm doing things or saying things that I don't want to because my anxiety is telling me that I have to in order to get along. It's learning how to build up a strong base within myself so that when I'm met with challenges I don't crumble or retaliate but can react and listen. It's having my boundaries and not trying to please everyone.

It's having difficult conversations; it's acknowledging when I've fucked up but also having boundaries.

It's about addressing the things that I've been putting off, ripping off the band-aid. It's also addressing that I have anxiety, or that I'm having a hard time, and understanding that I'm not a bad person because I'm having a hard time, and --- most importantly --- that I'm not the only person having a hard time. Not being able to ``thrive'' or ``manifest abundance'' in a fucked up world is not a sign of weakness.

\clearpage\null

\makesection{Social Media is not Your Friend, Or: \\Making Real Connections}

This desperate performative dance on social media bums me out and makes me anxious. I self-publish and I would like people to read what I write but I don't want to get tricked into believing, even now with lockdowns, that social media is about really connecting with people.

I don't believe what I see on it, I don't believe that people are content, I don't believe that they're using every second of this pandemic to be ‘productive' (and also if so, man, maybe think about the medical workers and the other equally as important staff in the hospitals, think of people risking their health to serve you, think of people who don't have the luxury to stay home, or the people who were laid off from their jobs and are struggling).

Maybe it's healthy for other people but I don't think it's healthy for me to have this pretence of human connection through platforms that are curated, filtered, and advertised on. I don't trust platforms that read our communication, encourage some people to profit from the platform but then ban sex workers and other people from making a living.

I do believe that we need to think about what we do online regarding social justice. It can be a tool for us, but it shouldn't be the only tool, and we shouldn't think that because we did this one thing online it should stop us from doing it offline.

I look at what I'm being offered online. Instead of thinking, why aren't I doing as well as this person, I ask instead, what am I getting from using this? Why am I being offered this in place of real connection and friendship? Why am I happy with perfected and perfectionist, curated, and public performance instead of the messiness and clumsiness and honesty and privacy that come with making real connections?

\clearpage\null

\makesection{I Loved to Be Numb, Or: \\Look at Your Drinking and Drug Use}

I'm a recovering/sober alcoholic and former frequent drug user and for me, I can only say that drinking and using made anxiety a lot worse. It helped it only while I was using, but the next day (or whenever I was next sober, depending on the bender) it all came back with a vengeance.

I was a stoner from about eighteen to twenty-one, and then I quit doing it full-time and congratulated myself as I was putting in endless hours of overtime from drinking and then using other drugs. I congratulated myself on quitting drinking and using drugs as I chain-smoked and binge-watched tv series for days at a time. I quit smoking and congratulated myself as I was doing the exact same thing with overeating and binge-watching even more series (or sometimes the same ones over again).

I checked out in different ways which, while not as harmful as drinking and using, were just as shitty for my mental health. These are still things that I find comforting and still things that I can allow, but I want to do it now and then, not as a result of bad things that I'm not dealing with. Generally, if I'm doing something with the same attitude that I was drinking or getting high with, then I need to cut it out and figure out what I'm trying to numb out with. There'll be plenty of time to be numb when I'm dead.

You know if your drinking and using is helping you. But if you're going around in circles, or if you notice that drinking makes your anxiety worse, consider cutting down, taking a break or quitting. If you can, it's a good idea to find a therapist, because a lot of pain comes up when you stop drinking and it helps to have someone to talk to about it.

If you're drinking only because you couldn't imagine what you'd do without drinking or thinking about how much of your social interactions are around alcohol or using, then question the social interactions! Why aren't there sober gigs? Sober clubs, sober places to hang out where we don't have to buy everything. Why are outdoor spaces so minimal and uncomfortable, with nowhere to sit in a group, nowhere to bring food, drink tea, and enjoy each other's company without consuming anything?

Oh, right. Consuming, consumption. Don't give us free pastimes, don't give us spaces outside to dissent. Look instead at who is telling us to endlessly consume; how gleeful the people in power are to throw us alcohol (while demonising drugs and ``the kind of people'' who use them).

That said, if drinking or using is working for you, if it's helping you and it's not hurting other people and it relieves stress, then it's not my place to tell you to stop! I have my ways of numbing out, and if I could drink booze and not immediately and actively try to fuck up my own life (and anyone else's in the immediate area) I would probably still drink as well.

I wonder what it's doing to us, though, on a personal and interpersonal level, what it's doing to us as a society or a movement, and what it's stopping us from doing. I just don't know how revolutionary getting fucked up at parties is when we're simply reflecting what everyone else does, and what's sold to us, and yeah, we should relax and unwind and rage, but a lot of us work during the week and there's precious little time to organise.%
\footnote{This will be explored more in an upcoming zine.}
I've been thinking of starting an alcoholics autonomous group here, to talk about why we drink, but also to get together and actively try to help each other with skills and solidarity. It could look like anxious groups getting together, or a signal group, or just admitting we feel this way, and while these reactions are totally understandable, they're getting in the way.

\clearpage\null

\makesection{Taking Your Meds, Or: \\Not Taking Your Meds}

There's no shame in taking medications (preferably prescribed by a health practitioner you trust, not someone who just slings pills at you without listening to your concerns). I probably have antidepressants to thank for stopping me drinking in 2017 --- I was taking Lexapro and drinking on top of them made me puke. Which, of course, I did multiple times before I decided that enough was enough.

I stopped taking Lexapro because I had some unwanted side effects and I was in therapy and had a good support system. There's so much weirdness about antidepressants, and in the beginning, I felt a bit ash\-am\-ed that I was taking them. Fuck that! If they help you, keep taking them. If anyone tries to shame you for taking them, tell them to stop taking painkillers when they have a headache or antihistamines when they have hay fever.

Shortly after reprinting and distributing this zine at the Stockholm Anarchist Book Fair in 2023, I felt like a total liar cos I was doing the worst I'd done mentally in a really long time. I went back on Lexapro after the panic attacks were coming in daily, despite all of the tools I had, and after two weeks of well, horror I'm starting to settle in and the panic is manageable again.

The things that I really think would help (no money concerns / no money, the chance to spend real time creatively instead of snatching moments, people and animals not being murdered and exploited constantly, oh yeah, and could we also not kill the earth?) are slightly out of reach, and while I agree with the idea that ``Trying to allay anxiety as a separate project from abolishing the conditions that create it is surely doomed''%
\footnote{A slightly cherry-picked quote from ``We Are All Very Anxious: Six Theses on Anxiety and Why It is Effectively Preventing Militancy, and One Possible Strategy for Overcoming It.'' The Institute for Precarious Consciousness.}
I think, as previously mentioned, staying at home to shake and cry isn't gonna get much abolishing done.

\clearpage\null

\makesection{The Mask Goes Over Your Nose, Or: \\Anxiety and Corona}

I thought that if I just controlled my behaviour enough, I wouldn't get sick. How could I get sick, when I was so cautious that it was making me anxious?

Despite all my hand washing, cutting off contacts, wearing masks, obsessing over the numbers and not sleeping, I got Corona at the start of November 2020. It'll be okay, I told myself, this time a little more forcefully, as I went through the symptoms, was told to stay inside for 22 days because I still had a cough. It'll be okay, I said, as the anxiety was coming up nearly every day.

I wasn't okay. When I finally got out of isolation and got home and saw my therapist, I had a really intense panic attack, followed by a week of almost constant high anxiety. All the weeks of anxiety came up because I didn't put it anywhere, and (because I'm lucky) I hadn't spent that much time before feeling both physically and mentally bad.

I was still telling myself that I should be recovering faster, writing more, and getting back to work. I told myself I shouldn't be this tired. I was so intent on pushing myself that I overlooked another health problem.

And look --- hopefully this audience is on the right side of this, but if you're lucky enough not to be in a high-risk group, wear a mask and wash your hands and keep away from people. It sucks! There isn't anyone who doesn't think this sucks. (Except, you know, Jeff Bezos --- the arsehole.)

\textbf{*2023:}
It's hard again, now, because everyone views Corona as being gone, or no big deal. I'm isolating a little, not wanting to be the only one with a mask, and also unwilling to not wear it anymore or meet as many people as I want to just because we want life to continue as it was. And look --- anxiety to me also means that my immune system is pretty fucked, I'm ill a lot. I've had Corona again since I last wrote this zine, and it knocked me out just as much. The willingness that people have now to spread this around is baffling.

\clearpage\null

\makesection{White Fragility, Or: \\Being Accountable}

I do let my anxiety stop me from doing some things. I don't want to go on rides, I don't want to go parachuting, I don't want to go scuba diving. I don't enjoy simulating danger for myself, because thank you, I'm able to do that in a supermarket or seeing an old lover on the street. I'm fine with ditching these things.

What I won't do, as a white person, is allow anxiety to be an excuse, or worse, a weapon, that stops me from having difficult conversations, and makes me turn the person who is questioning whatever behaviour I have into the aggressor.

White fragility, in particular the tears of white women, has harmed or killed so many BIPOC people. White fragility stops real community work from being done. It stops the affected people from being able to express their anger. It turns the argument from whatever issue there is to be about the white person. The discussion stops because the white person must be comfortable on all counts --- that is, they must have white supremacy upheld.

White fragility makes some people somehow think that it's acceptable to call the cops on BIPOC or less privileged people, or turn to legal processes because their comfort is, again, put above all else. This is unacceptable.

My anxiety and my panic attacks are very uncomfortable, but I am strong and capable of experiencing them. They cannot be used as a reason to stop listening, to not engage, and worse, to erase other's voices, or weaponise against BIPOC or other marginalised people.

At the same time, anxiety's present when I want to do shit. What if it's not good enough? What if I piss someone off? What if I'm not inclusive enough? For me --- and others might disagree --- I think it's more important to start and try and be willing to fuck up and learn and grow than to just idly (or, y'know, anxiously) witness what's going on. It also means standing up for yourself, making and exploring your own opinions, and challenging others.

\clearpage\null

\makesection{Transformation, Or: \\Using Our Tools For Good}

This zine is about self-soothing, but for the privileged people, it comes with a caveat. After we've self-soothed and we've managed to make ourselves feel better, we should think about the people who aren't in safe environments to make themselves feel better. People are trapped in prisons where the virus can run rampant. People trapped in their houses with their abusers, or with hateful relatives. People who have to work in unsafe conditions, people flown in from different countries to do the work deemed too dangerous for Germans to do. People with chronic health conditions whose health is at risk because arseholes who either are or march side by side with neo-Nazis won't wear masks, and won't keep their distance.

And by the way --- are these arseholes suffering from anxiety and panic attacks? I wonder.

\begin{figure}[hbt]
    \centering
    \includegraphics[width=\textwidth,keepaspectratio]{pine}
\end{figure}

Maybe I've subtly pointed out by now the fact that I do think a lot of anxiety comes from the way the world is. For the privileged people, it's about not being enough, needing to compare with others, lacking real connections, and striving only to make ourselves successful. For other people, it means not getting hired, working way too many hours for bullshit pay, not getting rented to, getting kicked out of the rental places, having to live in polluted areas from the wastes of capitalism, being beaten up, not eating, being exploited, being killed.

But I think the privileged people (myself very much included)%
\footnote{Since I started writing this zine, my mental, physical and financial health has plummeted, but I'm still privileged, which is wild.}
need to recognise that we're letting capitalism transform us. I don't believe if capitalism was magically taken away tomorrow we'd all be in some kind of utopia. There would still be power struggles, performative issues, demand for perfectionism, and a desire for individualism that's gone so far beyond recognising and wanting our rights that it's turned into believing that our comfort is more important than the rights of others, than the lives of others.

One of the main things that help me through anxiety and panic attacks is reaching out and being honest and being vulnerable. I want to view panic attacks and anxiety as warning signs that the way the world ‘works' now is by crushing most people to benefit few (while destroying the environment). Sometimes it seems like too much, and that things can't change, but who's saying this? Is it because we let anxiety tell us the things that we do aren't big enough? We can start small and work outwards.

We have to get together and reach out and support each other. Everyone I know either has or has experienced anxiety and panic attacks, but I still feel so alone when it happens, like nobody else has ever experienced it. They have. We can use this acknowledgment of things being fucked up to get together and try to change it.

\clearpage\null
\clearpage\null

\makesection{About Us}

Laura is a tattooer and multidisciplinary artist. She has a tattoo studio and art space called SóLaura Studio in Berlin. For tattoo bookings, artwork and other projects, visit \href{https://solaura.art}{solaura.art}, or her Instagram \mbox{\href{https://www.instagram.com/solauratattoo/}{@solauratattoo}}.

Jon is a teacher and writer.
Feel free to reach out and so we can (with deep, calming breaths) start some groups together.

In anxious solidarity,\\
Jon and Laura

\begin{figure}[h]
    \centering
    \includegraphics[width=\textwidth]{last-page}
\end{figure}
