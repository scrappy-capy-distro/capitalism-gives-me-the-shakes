DEFAULT_GOAL := help
OPEN = $(word 1, $(wildcard /usr/bin/xdg-open /usr/bin/open))
TARGET_IMPOSED = $(TARGET)-book

.PHONY: help
help: ## Print the help message
	@awk 'BEGIN {FS = ":.*?## "} /^[0-9a-zA-Z_-]+:.*?## / {printf "\033[36m%s\033[0m : %s\n", $$1, $$2}' $(MAKEFILE_LIST) | \
		sort | \
		column -s ':' -t

.PHONY: all
all: pdf pdf-imposed ## Create the all PDFs

.PHONY: pdf
pdf: $(TARGET).pdf ## Create the plain PDF

.PHONY: pdf-imposed
pdf-imposed: $(TARGET_IMPOSED).pdf ## Create the imposed PDF

.PHONY: $(TARGET).pdf
$(TARGET).pdf:
	xelatex -file-line-error -halt-on-error -shell-escape -interaction batchmode $(TARGET).tex && \
		xelatex -file-line-error -halt-on-error -shell-escape -interaction batchmode $(TARGET).tex

.PHONY: $(TARGET_IMPOSED).pdf
$(TARGET_IMPOSED).pdf:
	pdfbook2 -n -p a4paper -s $(TARGET).pdf

.PHONY: open
open: ## Open the PDF
	xdg-open $(TARGET).pdf

.PHONY: open-imposed
open-imposed: ## Open the imposed PDF
	xdg-open $(TARGET_IMPOSED).pdf
